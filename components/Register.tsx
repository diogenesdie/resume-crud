// pages/register.tsx (Registration Page)
import { useState, FormEvent } from 'react';
import InputMask from 'react-input-mask';

interface RegisterProps {
	// Add specific properties if needed
}

const Register: React.FC<RegisterProps> = () => {
	const [name, setName] = useState('');
	const [phone, setPhone] = useState('');
	const [email, setEmail] = useState('');
	const [webAddress, setWebAddress] = useState('');
	const [professionalExperience, setProfessionalExperience] = useState('');

	const handleRegistration = async (e: FormEvent) => {
		e.preventDefault();
		// call API to register user /api/resumes POST with axios
		const response = await fetch('/api/resumes', {
			method: 'POST',
			body: JSON.stringify({
				name,
				phone,
				email,
				website: webAddress,
				experience: professionalExperience,
			}),
			headers: {
				'Content-Type': 'application/json',
			},
		});

		const data = await response.json();

		const message = data.message;

		alert(message ? message : 'Registration Successful');
	};

	return (
		<div>
			<h1>Registration Form</h1>
			<form onSubmit={handleRegistration}>
				<label>
					Name:
					<input type="text" value={name} onChange={(e) => setName(e.target.value)} required />
				</label>
				<br />
				<label>
					Phone:
					<InputMask
						mask="(99) 99999-9999"
						value={phone}
						onChange={(e) => setPhone(e.target.value)}
					/>
				</label>
				<br />
				<label>
					Email:
					<input type="email" value={email} onChange={(e) => setEmail(e.target.value)} required />
				</label>
				<br />
				<label>
					Web Address:
					<input type="url" value={webAddress} onChange={(e) => setWebAddress(e.target.value)} />
				</label>
				<br />
				<label>
					Professional Experience:
					<textarea
						value={professionalExperience}
						onChange={(e) => setProfessionalExperience(e.target.value)}
						required
					></textarea>
				</label>
				<br />
				<button type="submit">Register</button>
			</form>
		</div>
	);
};

export default Register;
