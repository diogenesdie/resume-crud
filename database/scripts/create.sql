create sequence seq_resumes;

create table resumes
(
  resume_id  integer default nextval('seq_resumes'::regclass) not null
    constraint resumes_pk
      primary key,
  name       varchar(200)                                     not null,
  phone      varchar(11),
  email      varchar(100)                                     not null,
  website    varchar(100),
  experience text                                             not null
);
