// next.config.js
module.exports = {
  async headers() {
    return [
      {
        source: '/api/:path*', // Substitua por seu padrão de rota API
        headers: [
          { key: 'Content-Security-Policy', value: 'default-src https:; script-src https: ' }, // Exemplo de CSP, ajuste conforme necessário
          { key: 'X-Content-Type-Options', value: 'nosniff' }, // Evita que o navegador adivinhe o tipo MIME
        ],
      },
      {
        source: '/(.*)', // Aplica para todas as rotas
        headers: [
          { key: 'Strict-Transport-Security', value: 'max-age=63072000; includeSubDomains; preload' }, // HSTS para segurança de transporte
          { key: 'X-Frame-Options', value: 'DENY' }, // Evita ataques de clickjacking
          { key: 'X-XSS-Protection', value: '1; mode=block' }, // Proteção contra ataques XSS
        ],
      },
    ];
  },
};
