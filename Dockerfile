FROM node:18

# Create app directory
WORKDIR /opt/cv-experience

# Install app dependencies
COPY package*.json ./
COPY yarn.lock ./

RUN yarn install

# Bundle app source
COPY . .

# Build app
RUN yarn build

EXPOSE 3000
CMD [ "yarn", "start" ]
