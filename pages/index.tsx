import Form from '@/components/Register';
import { Inter } from 'next/font/google'
import { useSession, signIn, signOut } from "next-auth/react"
import Link from 'next/link';

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
	const { data: session } = useSession();

	if (!session) {
		return (
			<>
				Not signed in <br />
				<button onClick={() => signIn()}>Sign in</button>
			</>
		)
	}
	
	return (
		<main
			className={`flex min-h-screen flex-col items-center justify-between p-24 ${inter.className}`}
		>
			Signed in as {session?.user?.email} <br />
			<button onClick={() => signOut()}>Sign out</button>
			<Form />
			<Link href='/resumes'>
				View Resumes
			</Link>
		</main>
	)
}
