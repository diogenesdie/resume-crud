import dotenv from 'dotenv';
dotenv.config();

import { authOptions } from "@/pages/api/auth/[...nextauth]";
import type { NextApiRequest, NextApiResponse } from 'next';
import { unstable_getServerSession } from 'next-auth';
import { Client } from 'pg';
import validator from 'validator';


const handler = async (req: NextApiRequest, res: NextApiResponse) => {
	try {
		const session = await unstable_getServerSession(req, res, authOptions);

		if( !session ){
			throw {
				statusCode: 401,
				message: 'Unauthorized',
			};
		}

		const client = new Client({
			connectionString: process.env.DATABASE_URL,
		});
		client.connect();

		if (req.method === 'POST') {
			let { name, email, phone, website, experience } = req.body;

			name = validator.escape(validator.trim(name));
			email = validator.escape(validator.trim(email));
			phone = validator.escape(validator.trim(phone));
			website = validator.escape(validator.trim(website));
			experience = validator.escape(validator.trim(experience));

			if (validator.isEmpty(name) || validator.isEmpty(email) || validator.isEmpty(experience)) {
				throw {
					statusCode: 422,
					message: 'Fill the required fields',
				};
			}

			if (!validator.isEmail(email)) {
				throw {
					statusCode: 422,
					message: 'Invalid email',
				};
			}

			phone = phone.replace(/\D/g, '');

			const result = await client.query('INSERT INTO resumes (name, email, phone, experience, website) VALUES ($1, $2, $3, $4, $5) RETURNING *', [
				name,
				email,
				phone,
				experience,
				website,
			]);

			return res.status(200).json(result.rows[0]);

		} else if (req.method === 'GET') {
			const result = await client.query('SELECT * FROM resumes');
			return res.status(200).json(result.rows);
		}

	} catch (err: any) {
		return res.status(err.statusCode || 500).json({ statusCode: err.statusCode || 500, message: err.message });
	}
};
export default handler;
