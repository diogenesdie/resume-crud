import { Inter } from 'next/font/google'
import pg from 'pg'
import React from 'react';
import { useSession, signIn, signOut } from "next-auth/react"
import Link from 'next/link';

const inter = Inter({ subsets: ['latin'] })

export default function List({ resume, error }: any) {
	const { data: session } = useSession();
	
	if (!session) {
		return (
			<>
				Not signed in <br />
				<button onClick={() => signIn()}>Sign in</button>
			</>
		)
	}

	return (
		<main className={`flex min-h-screen flex-col items-center justify-between p-24 ${inter.className}`}>
			Signed in as {session?.user?.email} <br />
			<button onClick={() => signOut()}>Sign out</button>
			{error ? (
				<div>
					<h1>Something went wrong.</h1>
					<p>{error}</p>
				</div>
			) : (
                <div className='w-full'>
                    <h1 className='text-3xl'>Resume</h1>
                    <p>Name: {React.createElement('span', null, resume.name)}</p>
                    <p>Email: {React.createElement('span', null, resume.email)}</p>
                    <p>Phone: {React.createElement('span', null, resume.phone)}</p>
                    <p>Website: {React.createElement('span', null, resume.website)}</p>
                    <p>Experience: {React.createElement('span', null, resume.experience)}</p>
                </div>
			)}

			<Link href='/'>
				Back to Form
			</Link>
            <br/>
			<Link href='/resumes'>
				Back to List
			</Link>
		</main>

	)
}

export const getServerSideProps = async ({ locale, req, params, query }: any) => {
	try {
        const { id } = params;

		const client = new pg.Client({
			connectionString: process.env.DATABASE_URL
		});

		await client.connect();

		const { rows } = await client.query('SELECT * FROM resumes where resume_id = $1', [id]);

		await client.end();

        if( rows.length === 0 ) {
            return {
                notFound: true
            }
        }

		return {
			props: { resume: rows[0], error: null },
		};
	} catch (err: any) {
		return {
			props: {
				resumes: null,
				error: err.message,
			}
		};
	}
}