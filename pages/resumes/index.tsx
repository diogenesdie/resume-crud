import { Inter } from 'next/font/google'
import pg from 'pg'
import React from 'react';
import { useSession, signIn, signOut } from "next-auth/react"
import Link from 'next/link';

const inter = Inter({ subsets: ['latin'] })

export default function List({ resumes, error }: any) {
	const { data: session } = useSession();
	
	if (!session) {
		return (
			<>
				Not signed in <br />
				<button onClick={() => signIn()}>Sign in</button>
			</>
		)
	}

	return (
		<main className={`flex min-h-screen flex-col items-center justify-between p-24 ${inter.className}`}>
			Signed in as {session?.user?.email} <br />
			<button onClick={() => signOut()}>Sign out</button>
			{error ? (
				<div>
					<h1>Something went wrong.</h1>
					<p>{error}</p>
				</div>
			) : (
				<div className='w-full'>
					<h1 className='text-3xl'>Resumes</h1>
					<ul className='flex flex-col items-center justify-center w-full'>
						{resumes.map((resume: any) => (
							<li key={resume.resume_id} className='text-xl border-b-2 border-gray-300 py-2 my-2 w-full text-center'>
								<a href={`/resumes/${resume.resume_id}`}>
									{React.createElement('span', null, resume.name)} - <b>{React.createElement('span', null, resume.email)}</b>
								</a>
							</li>
						))}
					</ul>
				</div>
			)}
			<Link href='/'>
				Back to Form
			</Link>
		</main>

	)
}

export async function getServerSideProps() {
	try {
		const client = new pg.Client({
			connectionString: process.env.DATABASE_URL
		});

		await client.connect();

		const { rows } = await client.query('SELECT * FROM resumes');

		await client.end();

		return {
			props: { resumes: rows, error: null },
		};
	} catch (err: any) {
		return {
			props: {
				resumes: null,
				error: err.message,
			}
		};
	}
}